var fs = require('fs');
var server = require(argv.https ? 'https' : 'http');

var options = {}, httpServer;

if (argv.https) {
    options['key'] = fs.readFileSync(argv['https-key']);
    options['cert'] = fs.readFileSync(argv['https-cert']);
    if (argv['https-ca']) {
        options['ca'] = fs.readFileSync(argv['https-ca']);
    }
    httpServer = server.createServer(options).listen(argv['socket-port'], argv['bind-ip']);
} else {
    httpServer = server.createServer().listen(argv['socket-port'], argv['bind-ip']);
}

var redis = require("redis"),
    client = redis.createClient(argv['redis-port'], argv['redis-host']),
    clientPubSub = redis.createClient(argv['redis-port'], argv['redis-host']);

var io = require('socket.io')();
var elasticsearch = require('elasticsearch');

var elasticSearchOptions = {
    hosts: ['localhost:9200']
};

if (argv.https) {
    elasticSearchOptions = {
        hosts: ['10.9.176.66:9200', '10.9.176.67:9200']
    };
}


var esClient = new elasticsearch.Client(elasticSearchOptions);
var userIDToSocketID = {};
var chatUserIDToSocketID = {};

var REDIS_KEY_CONNECTED_USERS = 'users.connected';
var REDIS_KEY_CONNECTED_CHAT_USERS = 'users.connected.chat';

var REDIS_SUGGEST_READS_KEY = "suggest_num_reads";

client.del(REDIS_KEY_CONNECTED_USERS, REDIS_KEY_CONNECTED_CHAT_USERS);

function notify(userID, online) {
    client.sinter(REDIS_KEY_CONNECTED_CHAT_USERS, 'user.' + userID + '.friends', function (err, reply) {
        for (var index in reply) {
            var friendUserID = reply[index];

            for (var socketIndex in userIDToSocketID[friendUserID]) {
                var socketID = userIDToSocketID[friendUserID][socketIndex];
                io.to(socketID).emit('message', {
                    event: 'chat.friend.state.change',
                    data: {
                        friend: {
                            id: userID,
                            online: online
                        }
                    }
                });
            }
        }
    });
}

function stopChat(socket, userID, notifyInSeconds) {
    notifyInSeconds = notifyInSeconds || 0;
    if (chatUserIDToSocketID[userID]) {

        var socketIDIndex = chatUserIDToSocketID[userID].indexOf(socket.id);

        if (socketIDIndex > -1) {
            chatUserIDToSocketID[userID].splice(socketIDIndex, 1);
        }

        if (chatUserIDToSocketID[userID].length === 0) {
            client.srem(REDIS_KEY_CONNECTED_CHAT_USERS, userID);
            if (notifyInSeconds === 0) {
                notify(userID, false);
            } else {
                setTimeout(function () {
                    if (chatUserIDToSocketID[userID].length === 0) {
                        notify(userID, false);
                    }
                }, notifyInSeconds);
            }
        }
    }
}

io.on('connection', function (socket) {
    var userID = socket.userID;

    if (!userIDToSocketID[userID]) {
        userIDToSocketID[userID] = [];
    }

    userIDToSocketID[userID].push(socket.id);

    client.sadd(REDIS_KEY_CONNECTED_USERS, userID);

    socket.on('disconnect', function () {
        var socketIDIndex = userIDToSocketID[socket.userID].indexOf(socket.id);

        if (socketIDIndex > -1) {
            userIDToSocketID[socket.userID].splice(socketIDIndex, 1);
        }

        if (userIDToSocketID[socket.userID].length === 0) {
            client.srem(REDIS_KEY_CONNECTED_USERS, userID);
        }

        stopChat(socket, userID, 10000);
    });

    socket.on('chat.start', function (msg) {

        var userAllreadyInChat = false;

        if (!chatUserIDToSocketID[userID]) {
            chatUserIDToSocketID[userID] = [];
        } else {
            userAllreadyInChat = chatUserIDToSocketID[userID].length > 0;
        }

        chatUserIDToSocketID[userID].push(socket.id);

        client.sadd(REDIS_KEY_CONNECTED_CHAT_USERS, userID);

        // return friend list
        client.sinter(REDIS_KEY_CONNECTED_CHAT_USERS, 'user.' + userID + '.friends', function (err, reply) {

            if (reply.indexOf(userID) > -1) {
                reply.splice(reply.indexOf(userID), 1);
            }

            socket.emit('message', {
                event: 'chat.list.friends',
                data: {
                    friends: reply
                }
            });

        });

        if (!userAllreadyInChat) {
            notify(userID, true);
        }
    });

    socket.on('chat.stop', function () {
        stopChat(socket, userID);
    });

    socket.on('suggest', function (options) {
        options = JSON.parse(options);
        var query = options.data;

        esClient.suggest({
            index: 'won_suggester',
            body: {
                results: {
                    text: query,
                    completion: {
                        field: 'suggest',
                        context: {
                            type: ['user', 'artist', 'group']
                        }
                    }

                }
            }
        }, function (error, data) {

            if (data) {
                client.incr(REDIS_SUGGEST_READS_KEY);
            }

            var response = [];
            var hasHiddenGroup = false;

            if (data && data.results && data.results.length > 0 && data.results[0].options.length > 0) {
                for (var index in data.results[0].options) {
                    var option = data.results[0].options[index],
                        entry = option.payload;

                    entry.score = option.score;
                    if (entry.type === 'group' && entry.hidden) {
                        hasHiddenGroup = true;
                    }

                    response.push(entry);
                }
            }

            if (hasHiddenGroup) {
                client.smembers('user.' + socket.userID + '.memberships', function (err, memberships) {
                    memberships = memberships || [];

                    var filteredResponse = [];

                    for (var index in response) {
                        var entry = response[index];

                        if (!(entry.type === 'group' && entry.hidden)) {
                            filteredResponse.push(entry);
                        }

                        if (memberships.indexOf(entry.id + '') >= 0) {
                            filteredResponse.push(entry);
                        }

                    }

                    io.to(socket.id).emit('message', {
                            event: 'suggest',
                            data: {
                                query: query,
                                results: filteredResponse
                            }
                        }
                    );
                });

                return;
            }

            io.to(socket.id).emit('message', {
                    event: 'suggest',
                    data: {
                        query: query,
                        results: response
                    }
                }
            );
        });
    });
});

io.use(function (socket, next) {
    var token = socket.handshake.query.token;

    client.get('user.token.' + token, function (err, reply) {

        if (reply === null) {
            next(new Error('not authorized'));
        }

        socket.userID = reply;
        next();
    });
});

io.listen(httpServer);

clientPubSub.on("message", function (channel, message) {
    var decodedMessage = JSON.parse(message),
        flags = decodedMessage.flags,
        chatOnlyFlagSet = flags.indexOf(1) > -1;

    for (var index in decodedMessage.recipients) {
        var recipientID = decodedMessage.recipients[index];

        // recipient online?
        if (userIDToSocketID[recipientID] && userIDToSocketID[recipientID].length > 0) {

            for (var socketIndex in userIDToSocketID[recipientID]) {
                var socketID = userIDToSocketID[recipientID][socketIndex];

                if (chatOnlyFlagSet) {

                    // user socket chat enabled?
                    var userSocketChatting = chatUserIDToSocketID[recipientID] && chatUserIDToSocketID[recipientID].indexOf(socketID) > -1;
                    if (!userSocketChatting) {
                        continue;
                    }

                }

                io.to(socketID).emit('message', {
                        event: decodedMessage.identifier,
                        data: decodedMessage.payload
                    }
                );
            }
        }
    }

});

clientPubSub.subscribe("socket.updates");
