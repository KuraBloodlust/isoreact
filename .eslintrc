root: true
ecmaVersion: 7

ecmaFeatures:
  # enable arrow functions | () => {}
  arrowFunctions: true
  # enable binary literals | 0b0101010
  # binaryLiterals: true
  # enable let and const (aka block bindings)
  blockBindings: true
  # enable classes
  classes: true
  # enable default function parameters
  defaultParams: true
  # enable destructuring
  destructuring: true
  # enable for-of loops
  forOf: true
  # enable generators
  generators: true
  # enable modules and global strict mode
  # modules: true
  # enable computed object literal property names
  objectLiteralComputedProperties: true
  # enable duplicate object literal properties in strict mode
  objectLiteralDuplicateProperties: true
  # enable object literal shorthand methods
  objectLiteralShorthandMethods: true
  # enable object literal shorthand properties
  objectLiteralShorthandProperties: true
  # enable octal literals | 0o0123456
  octalLiterals: true
  # enable the regular expression u flag
  regexUFlag: true
  # enable the regular expression y flag
  regexYFlag: true
  # enable the rest parameters
  restParams: true
  # enable the spread operator for arrays
  spread: true
  # enable super references inside of functions
  superInFunctions: true
  # enable template strings
  templateStrings: true
  # enable code point escapes
  unicodeCodePointEscapes: true
  # allow return statements in the global scope
  # globalReturn: true
  # enable JSX
  # jsx: true
  # enable support for the experimental object rest/spread properties
  # experimentalObjectRestSpread: true
env:
  # browser global variables.
  # browser: true
  # Node.js global variables and Node.js scoping.
  # node: true
  # CommonJS global variables and CommonJS scoping (use this for browser-only code that uses Browserify/WebPack).
  # commonjs: true
  # web workers global variables.
  # worker: true
  # defines require() and define() as global variables as per the amd spec.
  # amd: true
  # adds all of the Mocha testing global variables.
  # mocha: true
  # adds all of the Jasmine testing global variables for version 1.3 and 2.0.
  # jasmine: true
  # Jest global variables.
  # jest: true
  # PhantomJS global variables.
  # phantomjs: true
  # Protractor global variables.
  # protractor: true
  # QUnit global variables.
  # qunit: true
  # jQuery global variables.
  # jquery: true
  # Prototype.js global variables.
  # prototypejs: true
  # ShellJS global variables.
  # shelljs: true
  # Meteor global variables.
  # meteor: true
  # MongoDB global variables.
  # mongo: true
  # AppleScript global variables.
  # applescript: true
  # Java 8 Nashorn global variables.
  # nashorn: true
  # Service Worker global variables.
  # serviceworker: true
  # Ember test helper globals.
  # embertest: true
  # WebExtensions globals.
  # webextensions: true
  # enable all ECMAScript 6 features except for modules.
  es6: true
#globals:
  #
#plugins:
  #
rules:
  # possible errors
  comma-dangle: [1, always-multiline]
  no-cond-assign: [2, except-parens]
  no-console: 1
  no-constant-condition: 2
  no-control-regex: 2
  no-debugger: 2
  no-dupe-args: 2
  no-dupe-keys: 2
  no-duplicate-case: 1
  no-empty-character-class: 1
  no-empty: 1
  no-ex-assign: 2
  no-extra-boolean-cast: 1
  no-extra-parens: [1, functions]
  no-extra-semi: 2
  no-func-assign: 2
  no-inner-declarations: [2, both]
  no-invalid-regexp: 2
  no-irregular-whitespace: 2
  no-negated-in-lhs: 2
  no-obj-calls: 2
  no-regex-spaces: 1
  no-sparse-arrays: 2
  no-unexpected-multiline: 1
  no-unreachable: 2
  use-isnan: 2
  valid-jsdoc:
    - 2
    - prefer:
        return: returns
        virtual: abstract
        augments: extends
        contructor: class
        const: constant
        defaultValue: default
        desc: description
        host: external
        fires: emits
        file: overview
        fileoverview: overview
        func: function
        method: function
        member: var
        arg: param
        argument: param
        prop: property
        exception: throws
        linkcode: link
        linkplain: link
  valid-typeof: 2

  # best practices
  accessor-pairs: 1
  block-scoped-var: 1
  complexity: [1, 10]
  consistent-return: 2
  curly: [2, multi-or-nest, consistent]
  default-case: 1
  dot-location: [2, property]
  dot-notation:
    - 2
    - allowKeywords: true
      # allowPattern: ""
  eqeqeq: [2, allow-null]
  guard-for-in: 1
  no-alert: 2
  no-caller: 2
  no-div-regex: 1
  no-else-return: 1
  no-empty-pattern: 2
  # no-eq-null: 1
  no-eval: 2
  no-extend-native: 2
  no-extra-bind: 1
  no-fallthrough: 2
  no-floating-decimal: 2
  # no-implicit-coercion: [1, { boolean: true, number: true, string: true}]
  no-implied-eval: 2
  no-invalid-this: 1
  no-iterator: 2
  no-labels: 2
  no-lone-blocks: 1
  no-loop-func: 1
  no-magic-numbers:
      - 2
      - { ignoreArrayIndexes: true, ignore: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10] }
  no-multi-spaces: 2
  no-multi-str: 2
  no-native-reassign: 2
  no-new-func: 1
  no-new-wrappers: 2
  no-new: 2
  no-octal-escape: 1
  no-octal: 2
  no-param-reassign:
    - 2
    - props: false
  no-process-env: 1
  no-proto: 2
  no-redeclare:
    - 2
    - builtinGlobals: false # allow redeclare of inbuilt globals
  no-return-assign: 2
  no-script-url: 2
  no-self-compare: 1
  no-sequences: 2
  no-unused-expressions: 2
  no-useless-call: 1
  no-useless-concat: 2
  no-void: 1
  no-warning-comments:
    - 1
    - terms:
        - todo
        - fixme
        - xxx
      location: "start"
  no-with: 2
  radix: 1
  vars-on-top: 2
  wrap-iife: [2, inside]
  yoda: [2, "never"]

  # strict mode
  strict: [2, global]

  # variables
  init-declarations: [1, always]
  no-catch-shadow: 2
  no-delete-var: 2
  no-label-var: 1
  no-shadow-restricted-names: 2
  no-shadow:
    - 1
    - builtinGlobals: false # allow overrides for builtin globals (eg Promise)
      hoist: "functions"
      allow:
        - done
        - cb
        - callback
  no-undef-init: 1
  no-undef:
    - 1
    - typeof: false
  no-undefined: 2
  no-unused-vars:
    - 2
    - vargs: all
      args: after-used
  no-use-before-define: 2

  # node/commonjs
  callback-return: 2
  global-require: 2
  handle-callback-err: 1
  no-mixed-requires:
    - 2
    - grouping: true
  no-new-require: 2
  no-path-concat: 1
  no-process-exit: 1
  #no-restricted-modules: [0, ["module", "names"]]#
  no-sync: 1

  # stylistic
  array-bracket-spacing: [2, never]
  block-spacing: [2, always]
  brace-style: [2, stroustrup]
  camelcase: [2, { properties: always }]
  comma-spacing:
    - 2
    - before: false
      after: true
  comma-style: [2, last]
  computed-property-spacing: [2, never]
  consistent-this: [2, self]
  eol-last: 2
  func-names: 1
  func-style: [2, declaration, { allowArrowFunctions: true }]
  id-length:
    - 2
    - min: 2
      max: 25
      properties: never
  # id-match: [2, pattern]
  indent: [2, "tab"]
  jsx-quotes: [1, prefer-double]
  key-spacing:
    - 2
    - align: value
      beforeColon: false
      afterColon: true
      mode: minimum
  linebreak-style: [2, unix]
  lines-around-comment:
    - 1
    - beforeBlockComment: true
      afterBlockComment: false
      beforeLineComment: true
      afterLineComment: false
      allowBlockStart: true
      allowBlockEnd: true
      allowObjectStart: true
      allowObjectEnd: true
      allowArrayStart: true
      allowArrayEnd: true
  max-nested-callbacks: [2, 3]
  new-parens: 2
  newline-after-var: [2, always]
  no-array-constructor: 2
  # no-continue: 1
  # no-inline-comments: 1
  no-lonely-if: 1
  no-mixed-spaces-and-tabs: [2, smart-tabs]
  no-multiple-empty-lines: 1
  no-negated-condition: 1
  no-nested-ternary: 2
  no-new-object: 2
  # no-restricted-syntax: [2, FunctionExpression, WithStatement]
  no-spaced-func: 2
  # no-ternary: 1
  no-trailing-spaces: 2
  no-underscore-dangle: 1
  no-unneeded-ternary: 1
  one-var: [2, never]
  operator-assignment: [2, always]
  operator-linebreak: [2, after]
  padded-blocks: [2, never]
  quote-props: [2, as-needed]
  quotes: [2, double, avoid-escape]
  require-jsdoc: 1
  # semi-spacing: [2]
  semi: [2, never]
  sort-vars: [1, { ignoreCase: true }]
  keyword-spacing:
      - error
      - {before: true, after: true}
  space-before-blocks: [2, always]
  space-before-function-paren: [2, { anonymous: always, named: never }]
  space-in-parens: [2, never]
  space-unary-ops:
    - 2
    - words: true
      nonwords: false
  spaced-comment: [2, always]
  # wrap-regex: 1

  # es6
  arrow-body-style: [1, as-needed]
  arrow-spacing: 2
  constructor-super: 2
  no-confusing-arrow: [2, {allowParens: true}]
  no-class-assign: 2
  no-const-assign: 2
  no-dupe-class-members: 2
  no-this-before-super: 2
  no-var: 2
  prefer-arrow-callback: 1
  prefer-const: 1
  prefer-reflect: 1
  prefer-spread: 2
  # prefer-template: 1
  require-yield: 1
