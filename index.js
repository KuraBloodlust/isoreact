"use strict"

const fs = require("fs")
const path = require("path")
const Broadway = require("broadway")
const bunyan = require("bunyan")
const etc = require("etc")()
etc.use(require("etc-yaml"))

const config = etc.all().toJSON()
const app = new Broadway(config)

// require all modules
const modulePath = path.join(__dirname, "lib", "init")
fs.readdirSync(modulePath).forEach(file => {
	try {
		// include module
		const mod = require(path.join(modulePath, file))

		// execute module
		mod(app)
	} catch (ex) {
		console.error("skipping " + file + "!", ex)
		if (process.env.NODE_ENV === "production") {
			return process.exit(1)
		}
	}
})

app.start(err => {
	if (err) {
		console.error("failed to start application", err)
		return process.exit(1)
	}

	setTimeout(() => {}, 10000)
})
