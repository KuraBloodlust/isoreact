"use strict"

module.exports = function flash(opts) {
	const key = opts && opts.key? opts.key: "flash"

	return function* (next) {
		const ctx = this
		const flashes = this.session[key]

		if (flashes) ctx.session[key] = null

	    this.flash = Object.seal({
	      get() { return flashes },
	      set(data) { ctx.session[key] = data },
	    })

	    return yield next()
	}
}
