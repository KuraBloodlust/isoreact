"use strict"

const path = require("path")
const Router = require("impress-router")

module.exports = app => {
	app.after("start", (app, config, next) => {
		const debug = process.env.NODE_ENV !== "production"
		const passport = require('koa-passport')
		const IO = require("koa-socket")

		const rootPath = path.join(__dirname, "..", "..")
		const logger = require('koa-bunyan-logger')

		require("koa-qs")(app.koa)
		require("koa-locale")(app.koa)
		require("koa-validate")(app.koa)

		app.use(require('koa-methodoverride')())

		app.use(logger(app.logger))
		app.use(logger.requestIdContext())
		app.use(logger.requestLogger())

		app.use(require("koa-toobusy")()) // return 503 if server to busy

		let store = null // memory storage by default
		if (config.redis.enabled) {
			// TODO sharding, pooling, clustering
			const storeConfig = { client: app.db.redis }

			// different db id for sessions
			if (config.redis.session && config.redis.session.id != config.redis.db) {
				storeConfig.duplicate = true
				storeConfig.db = config.redis.session.id
			}

			store = require("koa-redis")(storeConfig)
		}
		app.use(require("koa-generic-session")({ store }))

		app.use(require("koa-helmet")(Object.assign({
			contentSecurityPolicy: {
				// Specify directives as normal.
				directives: {
					defaultSrc: ["'self'"],
					scriptSrc: ["'self'", "'unsafe-inline'"],
					imgSrc: ["'self'", "data:"],
					sandbox: ["allow-forms", "allow-scripts"],
					objectSrc: [], // An empty array allows nothing through
					reportUri: "/_csp"
				},
				reportOnly: debug,
				setAllHeaders: true,
				browserSniff: true
			}, // filter requests for external domains, see http://content-security-policy.com/ for details
			xssFilter: true, // prevent a small subset of xss attacks
			frameguard: { action: "sameorigin" }, // dont enable frame embedding, except for myself
			hsts: false, // dont force http to https forwarding, let nginx handle it
			hidePoweredBy: { setTo: "PHP 3.6.9" }, // dont report powered by, report as php 3.6.9 instead
			ieNoOpen: true, // dont allow opening of downloads in context of the site
			noSniff: true, // dont do mime type sniffing. primarly internet explorer and script tags
			noCache: false, // dont force reload resources by default, enable browser cache
			hpkp: false, // dont do public key pinning by default, let nginx handle it
			dnsPrefetchControl: { allow: false } // dont do dns prefetching in supported browsers
		}, config.koa.cors)))

		app.use(require("koa-i18n")(app.koa, {
			directory: path.join(rootPath, "app", "translations"),
			extension: ".json",
			locales: config.environment.locals,
			modes: [ 'query', 'cookie', 'header', 'tld']
		}))


		app.use(require("koa-better-body")()) // parse body
		app.use(require("koa-csrf")()) // force csrf usage

		app.use(passport.initialize())
		app.use(passport.session()) // restore user account from session
		// https://github.com/nswbmw/koa-pass | https://github.com/yanickrochon/koa-rbac | https://github.com/Jackong/koa-acl

		app.use(require("koa-views")(path.join(rootPath, "app", "views"), {
			map: {
				nj: "nunjucks",
				twig: "nunjucks", // lazy, almost binary compatible
			}
		}))

		app.use(require('koa-static')(path.join(rootPath, "srv")))

		const router = new Router()
		app.perform("routes", router, done => done(), err => {
			if (err) return app.logger.error({ err }, "failed to initialize routes");

			// https://github.com/koajs/error#custom-templates
			app.use(require("koa-error")({
				template: path.join(rootPath, "app/views/error.html.nj")
			}))

			const io = new IO()
			io.attach(app.koa)

			return next()
		})
	})
}
