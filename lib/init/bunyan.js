"use strict"

/* eslint no-process-env: 0 */

const path = require("path")
const bunyan = require("bunyan")
const pkg = require("../../package")

module.exports = app => {
	const serializers = {
		err: bunyan.stdSerializers.err,
	}

	const logger = bunyan.createLogger({
		name:    pkg.name, // logger base name
		streams: [
			{ level: "trace", stream: process.stdout }, // stream all to console
			{ level: "warn", path: path.join(__dirname, "..", "..", "var", "error.log") }, // stream warn and error to error.log file
		],
		src:      process.env.NODE_ENV !== "production", // include source information in non-production environments - *attention* gathering src info takes time
		serializers, // include all defined serializers
	})

	// mixin logger interface into app
	app.mixin({
		createLogger: (moduleName, context) => logger.child(Object.assign({ module: moduleName }, context || {})),
		logger: logger
	})
}
