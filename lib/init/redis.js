"use strict"

const Promise = require("bluebird")
const redis = require("redis")
Promise.promisifyAll(redis.RedisClient.prototype)
Promise.promisifyAll(redis.Multi.prototype)

module.exports = app => {
	app.db = app.db || {}

	app.before("setup", (app, config, next) => {
		if (!config.redis.enabled) return next()
		const logger = app.createLogger({ module: "redis" })

		app.db.redis = redis.createClient({
			host: config.redis.host,
			port: config.redis.port,
			path: config.redis.path,
			password: config.redis.password,
			db: config.redis.db,
		})

		app.db.redis.on("ready", () => {
			logger.info("ready!")
		})

		app.db.redis.on("connect", () => {
			logger.info("connected!")
		})

		app.db.redis.on("reconnecting", () => {
			logger.info("disconnected, reconnecting!")
		})

		app.db.redis.on("warning", (err) => {
			logger.warn({ err }, "encountered a warning!")
		})

		app.db.redis.on("error", (err) => {
			logger.error({ err }, "encountered an error!")
		})

		return next()
	})
}
