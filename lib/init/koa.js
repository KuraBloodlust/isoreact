"use strict"

const Koa = require("koa")
const koa = new Koa()

module.exports = app => {
	app.mixin({
		use: koa.use.bind(koa),
		koa: koa,
	})

	app.before("setup", (_, config, done) => {
		const logger = app.createLogger("koa")
		const options = config.koa ||  {}

		if (!options.keys) logger.warn("no keys specified! please specify encryption keys in koa.keys")
		koa.keys = options.keys || ["notsoseekret"]
	})

	app.handle = koa.callback()
}
