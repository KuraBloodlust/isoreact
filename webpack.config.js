"use strict"

const webpack = require("webpack")
const ExtractTextPlugin = require("extract-text-webpack-plugin")
const HtmlWebpackPlugin = require("html-webpack-plugin");

const path = require("path")

const SRC_DIR = path.resolve(__dirname, "usr", "client")
const DST_DIR = path.resolve(__dirname, "public")

module.exports = {
    entry: {
        main: [
            path.resolve(SRC_DIR, "style/index.scss"),
            path.resolve(SRC_DIR, "script/index.js")
        ]
    },

    output: {
        path: DST_DIR,
        publicPath: "/",
        filename: "[name].bundle.js?[hash]",
        chunkFilename: "[id].bundle.js?[chunkhash]",
        pathinfo: true,
    },

    resolve: {
        extensions: ["", ".webpack.js", ".web.js", ".js", ".jsx"]
    },

    target: "web",

    bail: true,

    module: {
        loaders: [
            {
                // convert .jsx and .js by babel
                test: /\.jsx?/,
                include: SRC_DIR,
                loader: "babel-loader"
            },
            {
                // convert .scss by sass-loader
                test: /\.scss/,
                include: SRC_DIR,
                loader: ExtractTextPlugin.extract("style-loader", "css-loader!sass-loader")
            },
            {
                // copy over font files
                test: /\.(eot|svg|ttf|woff2?)/,
                loader: "file-loader?name=fonts/[name].[ext]?[hash]"
            }
        ]
    },

    plugins: [
        new HtmlWebpackPlugin({
            title: "symphonia bot interface",
            inject: false,
            template: require('html-webpack-template'),
            appMountId: 'app',
            mobile: true,
        }),
        new ExtractTextPlugin("[name].css?[hash]"),
        new webpack.ProvidePlugin({
            jQuery: "jquery",          // expected by bootstrap 4, doesnt use require
            "window.Tether": "tether", // expected by bootstrap 4, doesnt use require
        })
    ],

    sassLoader: {},

    stats:   {
        colors: true
    },
}
