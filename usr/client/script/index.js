"use strict"

import bootstrap from "imports?jQuery=jquery!bootstrap/dist/js/npm"

import React from "react"
import { render } from 'react-dom'

import routes from "./routes"


import { Router, Route, Link, browserHistory } from 'react-router'

import App from "./app"
import Login from "./login"

render((
    <Router history={browserHistory}>
        <Route path="/" component={App}>
            <Route path="login" components={{main: Login}}/>
        </Route>
    </Router>
), document.getElementById("app"))
